//
// Created by kyuubi09 on 3/30/2023.
//

#include "Game.h"

#include "Helpers.h"

#include "ServerFramework/ServerFramework.h"
#include "BSObj/BSObj.h"

#include "World/GameWorldMgr.h"
#include "ReferenceData/ReferenceDataMgr.h"

CGObjPC *CGame::GetCharObjSTAT(const char *pName) {
    if (g_pCGame == NULL)
        throw;

    return g_pCGame->GetCharObjByName(pName);
}

CGObjPC *CGame::GetCharObjSTAT(DWORD dwCharId) {
    if (g_pCGame == NULL)
        throw;

    return g_pCGame->GetCharObjById(dwCharId);
}

CGObjPC *CGame::GetCharObjJID_STAT(DWORD dwAccountJID) {
    if (g_pCGame == NULL)
        throw;

    return g_pCGame->GetCharObjByJID(dwAccountJID);
}

CGObj *CGame::GetGameObjSTAT(DWORD dwGameId) {
    if (g_pCGame == NULL)
        throw;

    return g_pCGame->GetGameObjById(dwGameId);
}

CGObjPC *CGame::GetCharObjByName(const char *pName) {
    std::list<std::pair<std::string, CGObjPC *>>::const_iterator it = m_listPcCharName.begin();
    for (; it != m_listPcCharName.end(); it++) {
        if ((*it).first.c_str() == pName)
            return ((*it).second);
    }

    return NULL;
}

CGObjPC *CGame::GetCharObjById(DWORD dwCharId) {
    std::map<DWORD, CGObjPC *>::const_iterator it = m_mapPcCharId.find(dwCharId);
    // If we got it
    if (it != m_mapPcCharId.end())
        return ((*it).second);

    return NULL;
}

CGObjPC *CGame::GetCharObjByJID(DWORD dwAccountJID) {
    std::map<DWORD, CGObjPC *>::const_iterator it = m_mapPcJID.find(dwAccountJID);
    // If we got it
    if (it != m_mapPcJID.end())
        return ((*it).second);

    return NULL;
}

CGObj *CGame::GetGameObjById(DWORD dwGameId) {
    std::list<std::pair<DWORD, CGObj *>>::const_iterator it = m_listObjGameId.begin();
    for (; it != m_listObjGameId.end(); it++) {
        if ((*it).first == dwGameId)
            return ((*it).second);
    }

    return NULL;
}

void CGame::ProcessMessage(CMsg *pMsg) {
    try {
        // Handle the server packets here
        switch (pMsg->GetMsgId()) {
            case SR_SHARDJOB_ACK: {
                WORD wShardMsgID;

                *pMsg >> wShardMsgID;
                if (wShardMsgID == SR_NOTIFY) {
                    OnJustShowError(*pMsg);
                    return;
                }
                // let's return it back
                pMsg->SetReadPos((pMsg->GetReadPos() - sizeof(wShardMsgID)));
            }
        }
    } catch (...) {
        pMsg->FlushRemainingBytes();
        WORD wMsgId = pMsg->GetMsgId();

        ServerFramework::ReportLog(LOG_TYPE_NOTIFY, " An Exception occurred in CGame::ProcessMessage() MsgID : %x",
                                   wMsgId);
        return;
    }
    reinterpret_cast<void (__stdcall *)(CGame *, CMsg *)>(0x00414260)(this, pMsg);
}

void CGame::OnJustShowError(CMsg &MSG) {
    WORD wNotifyType;
    CMsg *pAckMsg;
    DWORD dwRefUniqueID;

    MSG >> wNotifyType;
    switch (wNotifyType) {
        case SR_NOTIFY_ROC_QUEST: {
            std::string strQuestMsg;
            MSG >> strQuestMsg;

            pAckMsg = g_pNetEngine->AllocNewMsg(false);
            pAckMsg->SetMsgID(SR_NOTIFY);
            *pAckMsg << WORD(SR_NOTIFY_ROC_QUEST) << strQuestMsg;

            break;
        }
        case SR_NOTIFY_UNIQUE_KILLED: {
            std::string strUniqueKiller;

            MSG >> dwRefUniqueID >> strUniqueKiller;

            pAckMsg = g_pNetEngine->AllocNewMsg(false);
            pAckMsg->SetMsgID(SR_NOTIFY);
            *pAckMsg << WORD(SR_NOTIFY_UNIQUE_KILLED) << dwRefUniqueID << strUniqueKiller;

            std::string strUniqueCodeName("UnknownUnique");

            const RefObjCommon *pRefObj = REFDATA_MGR.GetRefObj(dwRefUniqueID);
            if (pRefObj != NULL)
                strUniqueCodeName = pRefObj->m_strObjectCode;

            BS_INFO("Unique Monster Killed! UNIQUE[%s] by [%s]", strUniqueCodeName.c_str(), strUniqueKiller.c_str())
            break;
        }
        case SR_NOTIFY_UNIQUE_SPAWN: {
            WORD wRegionID;
            float fUniquePosX;
            float fUniquePosY;
            float fUniquePosZ;

            MSG >> dwRefUniqueID >> wRegionID >> fUniquePosX >> fUniquePosY >> fUniquePosZ;

            pAckMsg = g_pNetEngine->AllocNewMsg(false);
            pAckMsg->SetMsgID(SR_NOTIFY);
            *pAckMsg << WORD(SR_NOTIFY_UNIQUE_SPAWN) << dwRefUniqueID;

            std::string strUniqueCodeName("UnknownUnique");

            const RefObjCommon *pRefObj = REFDATA_MGR.GetRefObj(dwRefUniqueID);
            if (pRefObj != NULL)
                strUniqueCodeName = pRefObj->m_strObjectCode;

            BS_INFO("Unique Monster Entered! UNIQUE[%s] POS[rid:%d (%.2f,%.2f,%.2f)]", strUniqueCodeName.c_str(),
                    wRegionID, fUniquePosX, fUniquePosY, fUniquePosZ)
            break;
        }
        default:
            return;
    }

    g_pGameWorldMgr->SendMsgToAllGameWorldsUser(pAckMsg);
}
