//
// Created by kyuubi09 on 3/30/2023.
//
#pragma once

#include "STL/SingletonT.h"

#include "Test/Test.h"
#include <list>
#include <map>

class   CGObjPC;
class   CGObj;
class   CMsg;

class CGame : public CSingletonT<CGame> {
public:
    virtual ~CGame();

public:
    static CGObjPC *GetCharObjSTAT(const char *pName);

    static CGObjPC *GetCharObjSTAT(DWORD dwCharId);

    static CGObjPC *GetCharObjJID_STAT(DWORD dwAccountJID);

    static CGObj *GetGameObjSTAT(DWORD dwGameId);

public:
    /// \brief Handling the packets
    ///
    /// \address 00414260
    ///
    void ProcessMessage(CMsg *pMsg);

private:
    CGObjPC *GetCharObjByName(const char *pName);

    CGObjPC *GetCharObjById(DWORD dwCharId);

    CGObjPC *GetCharObjByJID(DWORD dwAccountJID);

    CGObj *GetGameObjById(DWORD dwGameId);

private:
    /// \address 00414970 SR_SHARDJOB_ACK
    ///
    void OnShardJobAck(CMsg &MSG);

    /// \brief Handling OnGameNotify Msg (0x300c) came from shard.
    ///
    /// \address 00414970
    ///
    void OnJustShowError(CMsg &MSG);

private:
    char pad_0004[0x2240]; //0x0004
    std::list<std::pair<std::string, CGObjPC *>> m_listPcCharName; //0x2244
    char pad_2250[0x0040]; //0x2250
    std::map<DWORD, CGObjPC *> m_mapPcJID; //0x2290
    std::map<DWORD, CGObjPC *> m_mapPcCharId; //0x229C
    char pad_22A8[0x0010]; //0x22A8
    std::list<std::pair<DWORD, CGObj *>> m_listObjGameId; //0x22B8
    char pad_22C4[0x001C]; //0x22C4
private:
    BEGIN_FIXTURE()
        ENSURE_SIZE(0x22E0)
        ENSURE_OFFSET(m_listPcCharName, 0x2244)
        ENSURE_OFFSET(m_mapPcJID, 0x2290)
        ENSURE_OFFSET(m_mapPcCharId, 0x229C)
        ENSURE_OFFSET(m_listObjGameId, 0x22B8)
        END_FIXTURE()

    RUN_FIXTURE(CGame)
};

#define g_pCGame (*((CGame**)0x00d6a8e8))
