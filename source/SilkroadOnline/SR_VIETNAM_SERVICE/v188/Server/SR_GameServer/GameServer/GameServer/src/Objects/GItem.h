///******************************************************************************
/// \File GItem.h
///
/// \Desc
///
///******************************************************************************

#pragma once

#include "GObj.h"

class CGItem : public CGObj {
DECLARE_DYNAMIC_EXISTING(CGItem, 0x00adeb90)

public:
    virtual void Func_300();

    virtual void Func_301();

    virtual void Func_302();

    virtual void Func_303();

    virtual void Func_304();

    virtual void Func_305();

    virtual void Func_306();

    virtual void Func_307();

    virtual void Func_308();

    virtual void Func_309();

    virtual void Func_310();

    virtual void Func_311();

    virtual void Func_312();

    virtual void Func_313();

    virtual void Func_314();

    virtual void Func_315();

    virtual void Func_316();

    virtual void Func_317();

    virtual void Func_318();

    virtual void Func_319();

    virtual void Func_320();

    virtual void Func_321();

    virtual void Func_322();

    virtual void Func_323();

    virtual void Func_324();

private:
};