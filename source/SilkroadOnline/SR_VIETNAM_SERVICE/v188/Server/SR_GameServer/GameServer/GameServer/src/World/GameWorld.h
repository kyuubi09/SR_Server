///******************************************************************************
/// \File GameWorld.h
///
/// \Desc
///
///******************************************************************************

#pragma once

#include "Msg.h"

class CGameWorld {
public:
    virtual ~CGameWorld();

    virtual void Func_1();

    virtual void Func_2();

    virtual void Func_3();

    virtual void Func_4();

    virtual void Func_5();

    virtual void Func_6();

    virtual void Func_7();

    virtual void Func_8();

    virtual void Func_9();

    virtual void Func_10();

    virtual void Func_11();

    virtual void Func_12();

    virtual void Func_13();

    virtual void Func_14();

    virtual void Func_15();

    virtual void Func_16();

    virtual void Func_17();

    virtual void Func_18();

    virtual void Func_19();

    virtual void Func_20();

    virtual void Func_21();

    virtual void Func_22();

    virtual void Func_23();

    virtual void SendMsgToAllGameWorldUser(CMsg *pMsg);

    virtual void Func_25();

    virtual void Func_26();

    virtual void Func_27();

    virtual void Func_28();

    virtual void Func_29();

    virtual void Func_30();

    virtual void Func_31();

    virtual void Func_32();

    virtual void Func_33();

    virtual void Func_34();

    virtual void Func_35();

    virtual void Func_36();

    virtual void Func_37();

    virtual void Func_38();

    virtual void Func_39();

    virtual void Func_40();

    virtual void Func_41();

    virtual void Func_42();

    virtual void Func_43();

    virtual void Func_44();

    virtual void Func_45();

    virtual void Func_46();

    virtual void Func_47();

    virtual void Func_48();

    virtual void Func_49();

    virtual void Func_50();

    virtual void Func_51();

    virtual void Func_52();

    virtual void Func_53();

    virtual void Func_54();

};