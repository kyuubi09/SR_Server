//
// Created by kyuubi09 on 3/30/2023.
//

#include "GObjChar.h"

#include "StorageOP/GStorageOP.h"
#include "StorageOP/GPCInventoryOP.h"
#include "StorageOP/GNPCInventoryOP.h"
#include "StorageOP/GCOSInventoryOP.h"
#include "StorageOP/GChestOP.h"
#include "StorageOP/GGuildChestOP.h"
#include "StorageOP/GAvatarInventoryOP.h"
#include "StorageOP/GMagicCubeStorageOP.h"

IMPLEMENT_DYNAMIC_EXISTING(CGObjChar, 0x00adeb7c)

IStorage_ *CGObjChar::GetStorageOP(int nType) {
    switch (nType) {
        case NORMAL_STORAGE_TYPE:
            return (&(*reinterpret_cast<CGStorageOP *>(0x00c66a98)));
        case PC_INVENTORY_TYPE:
            return (&(*reinterpret_cast<CGPCInventoryOP *>(0x00c66aa0)));
        case NPC_INVENTORY_TYPE:
            return (&(*reinterpret_cast<CGNPCInventoryOP *>(0x00c66aac)));
        case COS_INVENTORY_TYPE:
            return (&(*reinterpret_cast<CGCOSInventoryOP *>(0x00c66ab8)));
        case CHEST_TYPE:
            return (&(*reinterpret_cast<CGChestOP *>(0x00c66ad0)));
        case GUILD_CHEST_TYPE:
            return (&(*reinterpret_cast<CGGuildChestOP *>(0x00c66adc)));
        case AVATAR_INVENTORY_TYPE:
            return (&(*reinterpret_cast<CGAvatarInventoryOP *>(0x00c66ac4)));
        case MAGIC_CUBE_STORAGE_TYPE:
            return (&(*reinterpret_cast<CGMagicCubeStorageOP *>(0x00c66ae8)));
        default:
            throw;
    }
}
