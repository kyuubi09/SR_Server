//
// Created by kyuubi09 on 3/30/2023.
//
#pragma once

#include "GObjMobile.h"

#include "IStorage.h"
#include "GStorage.h"

#include "CmdSrcNet.h"

class CGObjChar : public CGObjMobile {
DECLARE_DYNAMIC_EXISTING(CGObjChar, 0x00adeb7c)

private:
    char pad_0178[16]; //0x0178
    CCmdSrcNet *m_pSrcNet; //0x0188
    char pad_018c[6788]; //0x018C
    CGStorage m_PCInventory; //0x1C10
    char pad_1c34[108]; //0x1C34
public:
    void OnDeleteObject() override;

    void Func_214() override;

    void Func_125() override;

    void Func_172() override;

    void Func_215() override;

    void Func_219() override;

    void Func_194() override;

    void Func_211() override;

    void Func_299() override;

    void Func_300() override;

    void Func_156() override;

    void Func_227() override;

    void Func_293() override;

    void Func_292() override;

    void Func_291() override;

    void Func_124() override;

    void Func_197() override;

    void Func_198() override;

    void Func_303() override;

    MOTIONSTATE GetMotionState() const override;

    BODYMODE GetBodyMode() const override;

    double GetParam(GOBJ_PARAM wParamID) const override;

    int GetHealth() const override;

    int GetMana() const override;

    void Func_130() override;

    void Func_149() override;

    void Func_92() override;

public:
    virtual void Func_306();

    virtual void Func_307();

    virtual void Func_308();

    virtual void Func_309();

    virtual void Func_310();

    virtual void Func_311();

    virtual void Func_312();

    virtual void Func_313();

    virtual void Func_314();

    virtual void Func_315();

    virtual void Func_316();

    virtual void Func_317();

    virtual void Func_318();

    virtual void Func_319();

    virtual void Func_320();

    virtual void Func_321();

    virtual void Func_322();

    virtual void Func_323();

    virtual void Func_324();

    virtual void Func_325();

    virtual void Func_326();

    virtual void Func_327();

    virtual void Func_328();

    virtual void Func_329();

    virtual void Func_330();

    virtual void Func_331();

    virtual void Func_332();

    virtual void Func_333();

    virtual void Func_334();

    virtual void Func_335();

    virtual void Func_336();

    virtual void Func_337();

    virtual void Func_338();

    virtual void Func_339();

    virtual void Func_340();

    virtual void Func_341();

    virtual void Func_342();

    virtual void Func_343();

    virtual void Func_344();

    virtual void Func_345();

    virtual void Func_346();

    virtual void Func_347();

    virtual void Func_348();

    virtual void Func_349();

    virtual void Func_350();

    virtual void Func_351();

    virtual void Func_352();

    virtual void Func_353();

    virtual void Func_354();

    virtual void Func_355();

    virtual void Func_356();

    virtual void Func_357();

    virtual void Func_358();

    virtual void Func_359();

    virtual void Func_360();

    virtual void Func_361();

    virtual void Func_362();

    virtual void Func_363();

    virtual void Func_364();

    virtual void Func_365();

    virtual void Func_366();

    virtual void Func_367();

    virtual void Func_368();

    virtual void Func_369();

    virtual void Func_370();

    virtual void Func_371();

    virtual void Func_372();

    virtual void Func_373();

    virtual void Func_374();

    virtual void Func_375();

    virtual void Func_376();

    virtual void Func_377();

    virtual void Func_378();

    virtual void Func_379();

    virtual void Func_380();

    virtual void Func_381();

    virtual void Func_382();

    virtual void Func_383();

    virtual void Func_384();

    virtual void Func_385();

    virtual void Func_386();

    virtual void Func_387();

    virtual void Func_388();

    virtual void Func_389();

    virtual void Func_390();

    virtual void Func_391();

    virtual void Func_392();

    virtual void Func_393();

    virtual void Func_394();

    virtual void Func_395();

    virtual void Func_396();

public:
    static IStorage_ *GetStorageOP(int nType);

private:
BEGIN_FIXTURE()
        ENSURE_SIZE(0x1CA0)
        ENSURE_OFFSET(m_PCInventory, 0x1C10)
    END_FIXTURE()

    RUN_FIXTURE(CGObjChar)
};