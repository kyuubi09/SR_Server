//
// Created by Kurama on 12/19/2022.
//

#include "Util.h"

#include "memory/hook.h"

#include "BSObj/BSObj.h"

#include "MainProcess.h"

void Init() {
#ifdef CONFIG_DEBUG_CONSOLE
    AllocConsole();
    freopen("CONOUT$", "w", stdout);
    freopen("CONIN$", "r", stdin);
#endif // CONFIG_DEBUG_CONSOLE

    BS_INFO("SRO_Server-Kit, SR_GameServer.");
    BS_INFO("******************************************************************************")

    vftableHook(0x00adeffc, 9, addr_from_this(&CMainProcess::_OnProcessMessage));
}